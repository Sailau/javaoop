package week3.StrategyPattern;

public interface PayStrategy {
    boolean pay(int paymentAmount);
    void collectPaymentDetails();
}
