package week3.ObserverPattern;

import java.util.List;

public interface Observer {
        void update(List<String> vacancies);
}
