package week3.ObserverPattern;

public class Magazine {
    public static void main(String[] args) {
        Main headHunter=new Main();
        headHunter.addVacancy("JJD");
        headHunter.addVacancy("HSD");
        headHunter.addVacancy("C++");

        ConcreteObserver alisher=new ConcreteObserver("alisher");
        ConcreteObserver Venera= new ConcreteObserver("Venera");

        headHunter.addObserve(alisher);
        headHunter.addObserve(Venera);

        headHunter.addVacancy("GLD");
        headHunter.removeObserver(alisher);

        headHunter.removeVacancy("C++");

    }
}
