package week3.ObserverPattern;

import java.util.ArrayList;
import java.util.List;

public class Main implements Subject{
   private List<Observer> ConcreteObservers;
   private List<String> vacancies;

    public Main() {
        ConcreteObservers=new ArrayList<>();
        vacancies=new ArrayList<>();
    }

    public void addVacancy(String vacancy){
        vacancies.add(vacancy);
        NotifyObserver();
    }

    public void removeVacancy(String vacancy){
        vacancies.remove(vacancy);
        NotifyObserver();
    }


    @Override
    public void addObserve(Observer obs) {
ConcreteObservers.add(obs);
    }

    @Override
    public void removeObserver(Observer obs) {
ConcreteObservers.remove(obs);
    }

    @Override
    public void NotifyObserver() {
        for (Observer obs: ConcreteObservers) {
            obs.update(vacancies);
        }
    }


}
