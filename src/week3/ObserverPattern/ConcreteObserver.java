package week3.ObserverPattern;


import javax.swing.plaf.IconUIResource;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class ConcreteObserver implements Observer{

     String name;
    public ConcreteObserver(String name) {
        this.name = name;
    }


    @Override
    public void update(List<String> vacancies) {
        System.out.println("Dear "+ name +" we have changes");
        for (String element: vacancies) {
            System.out.println(element);
        }
    }


}
