package week3.ObserverPattern;

public interface Subject {
   void addObserve(Observer obs);
   void removeObserver(Observer obs);
   void NotifyObserver();
}
