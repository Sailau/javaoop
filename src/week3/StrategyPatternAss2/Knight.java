package week3.StrategyPatternAss2;

public class Knight extends Character {
    @Override
    void fight() {
        System.out.println("I am Knight!");
    }

    public Knight() {
        weapon=new AxeBehavior();
    }
}
