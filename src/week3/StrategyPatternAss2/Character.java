package week3.StrategyPatternAss2;

public abstract class Character {
    WeaponBehavior weapon;
    void fight() {}

    public void setWeapon(WeaponBehavior w) {
        this.weapon = w;
    }

    public void Attack(){
        weapon.useWeapon();
    }

}
