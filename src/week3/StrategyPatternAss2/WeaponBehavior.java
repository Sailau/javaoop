package week3.StrategyPatternAss2;

public interface WeaponBehavior {
    void useWeapon();
}
