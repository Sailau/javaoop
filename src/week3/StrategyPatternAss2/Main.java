package week3.StrategyPatternAss2;

public class Main {
    public static void main(String[] args) {
        Character A1=new King();
        A1.fight();

        Character A2=new Queen();
        A2.fight();
        A2.Attack();
        A2.setWeapon(new AxeBehavior());
        A2.Attack();
    }

}
