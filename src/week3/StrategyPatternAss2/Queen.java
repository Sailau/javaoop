package week3.StrategyPatternAss2;

public class Queen extends Character {
    @Override
    void fight() {
        System.out.println("I am Queen");
    }

    public Queen() {
        weapon = new BowAndArrowBehavior();
    }

}
