package week3.StrategyPatternAss2;

public class AxeBehavior implements WeaponBehavior {
    @Override
    public void useWeapon() {
        System.out.println("Axe");
    }
}
