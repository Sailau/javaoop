package week3.StrategyPatternAss2;

public class Troll extends Character {
    @Override
    void fight() {
        System.out.println("Just Troll");
    }

    public Troll() {
        weapon=new KnifeBehavior();
    }
}
