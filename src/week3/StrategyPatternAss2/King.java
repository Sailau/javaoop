package week3.StrategyPatternAss2;

public class King extends Character {
    @Override
    void fight() {
        System.out.println("I am King!");
    }

    public King() {
        weapon=new SwordBehavior();
    }

}
