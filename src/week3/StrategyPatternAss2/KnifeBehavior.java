package week3.StrategyPatternAss2;

public class KnifeBehavior implements WeaponBehavior {

    @Override
    public void useWeapon() {
        System.out.println("Knife");
    }
}
