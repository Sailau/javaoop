package week1;

public class Employee {
    private  String name;
    private double rate;
    private int hours;
    static double totalSum;

    public double getRate() {
        return rate;
    }
    public int getHours() {
        return hours;
    }
    public String getName() {
        return name;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setRate(double rate) {
        this.rate = rate;
    }
    public static void setTotalSum(double totalSum) {
        Employee.totalSum = totalSum;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", rate=" + rate +
                ", hours=" + hours +
                '}';
    }

    public Employee(){}
    public Employee(String name, double rate){
        this.name=name;
        this.rate=rate;
    }
    public Employee(String name, double rate, int hours){
        this.name=name;
        this.rate=rate;
        this.hours=hours;
    }
    public double salary(){
        return rate*hours;
    }

    public void changeRate(double rate){
        this.rate=rate;
    }
    public double bonus(){
        return  salary()*0.1;
    }
    public double getTotalSum(){
        totalSum = bonus() + salary();
        return totalSum;
    }

}
