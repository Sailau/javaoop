package week1.OPPinImages;

import javax.swing.plaf.IconUIResource;

public class Duck {
    Quackable quackBehavior;
    Flyable flyBrhavior;

    public void performQuack(){
        quackBehavior.quack();
    }
    public void performFly(){
        flyBrhavior.fly();
    }

    public void swim(){
System.out.println("I am swimming");
    }

    public void quack(){
        System.out.println("I am quacking");
    }

    public void display(){
        System.out.println("I am a duck");
    }


}
