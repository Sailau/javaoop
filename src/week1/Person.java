package week1;

import javax.swing.*;

public class Person {
    private  String name;
    private int birthYear;

    public void setName(String name) {
        this.name = name;
    }
    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public String getName() {
        return name;
    }
    public int getBirthYear() {
        return birthYear;
    }

    public Person(){}
    public Person(String name, int birthYear){
    }

    public int Age(){
        return 2020-birthYear;
    }
    public void Input(String name, int birthYear){
        this.name=name;
        this.birthYear=birthYear;
    }
    public String Output(){
        return "week1.Person:[name="+name+",age="+Age()+"]";
    }
    public void changeName(String name){
        this.name=name;
    }
}
